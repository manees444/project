package alquranapps.islamapps.applicationtest;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import alquranapps.islamapps.applicationtest.Models.FoodModel;

public class FoodsActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    private List<FoodModel> foodListItem = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        recyclerView = findViewById(R.id.recyvlerView);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(new FoodAdapter(this, foodListItem));

        addFoodEntries();

    }

    public void addFoodEntries() {
        FoodModel foodModel;
       /* foodModel = new FoodModel("Bryani", "C130", "130");
        foodListItem.add(foodModel);
        foodModel = new FoodModel("Bar B Q", "149", "1830");
        foodListItem.add(foodModel);
        foodModel = new FoodModel("Burger", "100", "130");
        foodListItem.add(foodModel);
        foodModel = new FoodModel("Shawarma", "100", "1300");
        foodListItem.add(foodModel);
        foodModel = new FoodModel("Pizza", "C800", "130");
        foodListItem.add(foodModel);
        foodModel = new FoodModel("Rice", "100", "130");
        foodListItem.add(foodModel);
*/
    }

    private class FoodAdapter extends RecyclerView.Adapter<ViewHolder> {
        private List<FoodModel> foodList;
        Context context;

        public FoodAdapter(Context context, List<FoodModel> foodList) {
            this.context = context;
            this.foodList = foodList;
            Log.e("Food List C", foodList.toString());
        }


        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(context).inflate(R.layout.food_listitems, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
            holder.itemView.requestLayout();
            final FoodModel foodModel = foodList.get(position);
            holder.foodName.setText(foodModel.getFoodName());
//            holder.imageView.setImageResource(foodModel.getImage());
            holder.foodPrice.setText((int) foodModel.getPrice());
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(context, foodModel.getFoodName() + "--" + foodModel.getPrice(), Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(FoodsActivity.this, OrderActivty.class);
                    intent.putExtra("FName", foodModel.getFoodName());
                    intent.putExtra("FPrice", foodModel.getPrice());
                    startActivity(intent);
                }
            });
            Log.e("Food List", foodList.toString());

        }

        @Override
        public int getItemCount() {
            return foodList.size();
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView foodName, foodPrice;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.imageFood);
            foodName = itemView.findViewById(R.id.foodName);
            foodPrice = itemView.findViewById(R.id.fPrice);
        }
    }
}
