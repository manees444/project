package alquranapps.islamapps.applicationtest.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class ProjectDatabase extends SQLiteOpenHelper {

    static String databaseName = "Project.db";
    String tableName = "User";
    String col_id = "id";
    String col_fullName = "FullName";
    String col_email = "Email";
    String col_username = "Username";
    String col_password = "Password";

    String food_tableName = "FoodItems";
    String food_col_id = "id";
    String food_col_foodName = "FoodName";
    String food_col_category = "Category";
    String food_col_price = "Price";

    public ProjectDatabase(Context context) {
        super(context, databaseName, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table " + tableName + "(" +
                col_id + " integer primary key autoincrement, " +
                col_fullName + " text, " +
                col_email + " text, " +
                col_username + " text, " +
                col_password + " text)"
        );
        db.execSQL("create table " + food_tableName + "(" +
                food_col_id + " integer primary key autoincrement, " +
                food_col_foodName + " text, " +
                food_col_category + " text, " +
                food_col_price + " text)"
        );
        Log.e("Database", "onCreate");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //db.execSQL("DROP TABLE IF EXISTS " + tableName);
        //db.execSQL("DROP TABLE IF EXISTS " + food_tableName);
        onCreate(db);
        Log.e("Database", "onUpgrade");

    }

    //    start Login or Register
    public long addUser(String fullName, String email, String userName, String password) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("fullName", fullName);
        contentValues.put("email", email);
        contentValues.put("username", userName);
        contentValues.put("password", password);
        long res = db.insert(tableName, null, contentValues);
        db.close();
        Log.e("addUser res", String.valueOf(res));
        return res;
    }

    public boolean checkUser(String user, String pass) {
        String[] column = {col_id};
        SQLiteDatabase db = getReadableDatabase();
        String selection = col_username + "=?" + " and " + col_password + "=?";
        String[] args = {user, pass};
        Cursor cursor = db.query(tableName, column, selection, args, null, null, null);
        int count = cursor.getCount();
        cursor.close();
        db.close();
        Log.e("checkUser cursor", String.valueOf(cursor));

        if (count > 0) {
            return true;
        } else
            return false;
    }
    //   Close Login or Register
    //    Start Food Database

    public long addFood(String FoodName, String Category, String Price) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("FoodName", FoodName);
        contentValues.put("Category", Category);
        contentValues.put("Price", Price);
        long res = db.insert(food_tableName, null, contentValues);
        db.close();
        Log.e("Food res", String.valueOf(res));
        return res;
    }

}
