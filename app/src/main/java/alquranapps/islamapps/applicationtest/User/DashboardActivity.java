package alquranapps.islamapps.applicationtest.User;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import alquranapps.islamapps.applicationtest.FoodsActivity;
import alquranapps.islamapps.applicationtest.R;

public class DashboardActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    RecyclerAdapter recyclerAdapter;
    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        recyclerView = findViewById(R.id.recyvlerView);
        recyclerAdapter = new RecyclerAdapter(this);
        textView = findViewById(R.id.usernameText);

        String user = getIntent().getStringExtra("Username");
        textView.setText("Welcome\n" + user);

        RecyclerView.LayoutManager manager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(manager);
        recyclerView.setAdapter(recyclerAdapter);
    }

    public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {
        Context context;
        String[] meals = {"Fresh Meal", "Fruits and Vegatable", "Grocery", "Bavarages", "Personal Care"};
        int[] mealImage = {R.drawable.meal1, R.drawable.meal2, R.drawable.meal3, R.drawable.meal4, R.drawable.meal5};

        public RecyclerAdapter(Context context) {
            this.context = context;

        }

        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(context).inflate(R.layout.list_items, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
            holder.itemView.requestLayout();
            holder.textView.setText(meals[position]);
            holder.imageView.setImageResource(mealImage[position]);
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(context, meals[position], Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(DashboardActivity.this, FoodsActivity.class);
                    startActivity(intent);

                }
            });
        }

        @Override
        public int getItemCount() {
            return meals.length;
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            ImageView imageView;
            TextView textView;

            public ViewHolder(@NonNull View itemView) {
                super(itemView);
                imageView = itemView.findViewById(R.id.imageView);
                textView = itemView.findViewById(R.id.textView);
            }
        }
    }
}
